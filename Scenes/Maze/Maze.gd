extends Node

class_name Maze

const N = 1
const E = 2
const S = 4
const W = 8

const North = Vector2(0,-1)
const South = Vector2(0,1)
const West = Vector2(-1,0)
const East = Vector2(1,0)

var cell_walls = {North: N, East: E, South: S,West: W}

var tile_array_name_indexed = []
var tile_size = 0
var maze_height = 0
var maze_width = 0
var Map = null
var parent_maze = null
var maze_handler = null

var paths = [[Vector2(0,0)]]
#starting location in tiles
var x0 = 0
var y0 = 0

#ending location in tiles
var xf = 0
var yf = 0
var path_to_exit = []

var id = 0

func _init(maze_width, maze_height, x, y, tile_array_name_indexed, Map, maze_handler, parent_maze, id):
	self.maze_height = maze_height
	self.maze_width = maze_width
	x0 = x
	y0 = y
	self.tile_array_name_indexed = tile_array_name_indexed
	self.Map = Map
	self.maze_handler = maze_handler
	self.parent_maze = parent_maze
	self.id = id
	
	
	tile_size = Map.cell_size
	make_maze()


func check_neightbors(cell, unvisited):
	#returns an array of cell's unvisited neighbors
	var list = []
	for n in cell_walls.keys():
		if cell + n in unvisited:
			list.append(cell + n)
	return list


func make_maze():
	var unvisited = []
	var stack = []
	Map.clear()
	
	# fill unvisited with every cell and set them all the the default tile
	for x in range(maze_width):
		for y in range(maze_height):
			unvisited.append(Vector2(x,y))
			Map.set_cellv(Vector2(x,y),tile_array_name_indexed[N|S|E|W])
			
	
	var current = Vector2(x0,y0)
	unvisited.erase(current)
	
	var new_path = true
	var temp_current = current
	var paths_index = 0
	
	paths[paths_index] = [current]
	
	while unvisited:															# There are unvisited cells
		var neighbors = check_neightbors(current,unvisited)
		if neighbors.size() > 0:
			
			new_path = true 													# there is a new path, i.e. not backtracking
					
			var next = neighbors[randi() % neighbors.size()]					# randomly selects neighbor as next cell to travel to
			stack.append(current)												# adds the current cell to the stack
			var dir = next - current											# vector of unit length 1 pointing from current cell to next cell
			
			# fancy math for matching tile to tile name
			var current_walls = int(Map.tile_set.tile_get_name(Map.get_cellv(current))) - cell_walls[dir]
			var next_walls = int(Map.tile_set.tile_get_name(Map.get_cellv(next))) - cell_walls[-dir]
			self.Map.set_cellv(current,tile_array_name_indexed[current_walls])
			self.Map.set_cellv(next,tile_array_name_indexed[next_walls])
			
			# move to next cell
			current = next
			unvisited.erase(current)
			
			# add the current cell to the path
			paths[paths_index].append(current)
			
		elif stack:																# There are cells to be checked for more neighbors
			if new_path:														# A path has concluded
				paths_index += 1
				paths.append(paths[paths_index - 1].duplicate())				# duplicate the previous path
				new_path = false
			paths[paths_index].pop_back()										# current path's index is removed as the alg backtracks
			current = stack.pop_back()											# current cell is the next furthest to check for more branches
			
# 	Sets (xf, xy) to the cell furthest from (x0, y0)
#	var maxDist = -1
#	for x in range(maze_width):
#		for y in range(maze_height):
#			var name = int(Map.tile_set.tile_get_name(Map.get_cell(x, y)))
#			if name == 7 or name == 11 or name == 13 or name == 14:
#				if(sqrt(pow(x-x0, 2) + pow(y-y0,2)) > maxDist):
#					maxDist = sqrt(pow(x-x0, 2) + pow(y-y0,2))
#					xf = x
#					yf = y

	# Set (xf, yf) to cell that has the longest path
	var max_len = -1
	for e in paths:
		if(e.size() > max_len):
			max_len = e.size()
			xf = e[e.size()-1].x
			yf = e[e.size()-1].y
			path_to_exit = e
		
func get_Map():
	return Map

func get_Parent_Maze():
	return parent_maze
