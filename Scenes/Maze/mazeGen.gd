extends Node


var x0 = 0
var y0 = 0

const N = 1
const E = 2
const S = 4
const W = 8

var cellWalls = {Vector2(0, -1): N, Vector2(1, 0): E, Vector2(0, 1):S, Vector2(-1, 0):W}

var tileSize = 32
var width = 25
var height = 15

onready var map = $TileMap


func _init(x, y):
	x0 = x
	y0 = y
	tileSize = map.cell_size
			
func checkNeighbors(cell, unvisited):
	var ls = []
	for n in cellWalls.keys():
		if cell + n in unvisited:
			ls.append(cell+n)			
	return ls


func createMaze():
	var unvisited = []
	var stack = []
	
	for i in range(width):
		for j in range(height):
			unvisited.append(Vector2(i, j))
			map.set_cellv(Vector2(i, j), N|E|S|W)
	
	var current = Vector2(x0, y0)
	unvisited.erase(current)

	while unvisited:
		var neighbors = checkNeighbors(current, unvisited)
		if neighbors.size() > 0:
			var next = neighbors[randi() % neighbors.size()]
			stack.append(current)
			#remove walls
			var dir = next - current
			var currentWalls = map.get_cellv(current) - cellWalls[dir]
			var nextWalls = map.get_cellv(next) - cellWalls[-dir]
			
			map.set_cellv(current, currentWalls)
			map.set_cellv(next, nextWalls)
			
			current = next
			unvisited.erase(current)
		elif stack:
			current = stack.pop_back()
		
		yield(get_tree(), 'idle_frame')	
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
