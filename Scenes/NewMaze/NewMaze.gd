extends Node2D


# Configurable variables from outside the instance
export var maze_height: int = 9
export var maze_width : int = 9

# Origin cell of the maze
var x0 = 0
var y0 = 0

#Exit cell of the maze
var xf = 0
var yf = 0

# Directional Constant
const N = 1
const E = 2
const S = 4
const W = 8
var cell_walls = {Vector2(0, -1): N, Vector2(1, 0): E, Vector2(0, 1):S, Vector2(-1, 0):W}

#Reference to the parent maze
var parent_maze: Node = null

#Our active tileMap
onready var tileMap : TileMap = $TileMap
var tile_size = null
var maze_path_wall_thickness = 8

#Path tracking variables
var paths = [[Vector2(0,0)]]
var path_to_exit = []

#Reference to the submazes
onready var viewportMaze = load("res://Scenes/Camera/SubMazeCamera.tscn")
onready var maze = load("res://Scenes/NewMaze/NewMaze.tscn")
var subMazes = []

#Reference to the player
onready var Player = load("res://Scenes/Player/Player.tscn")
var player = null
export var active : bool = false



#Reference to the maze exits
onready var rightExit = load("res://Scenes/NewMaze/RightExit.tscn")
onready var leftExit = load("res://Scenes/NewMaze/LeftExit.tscn")
onready var topExit = load("res://Scenes/NewMaze/TopExit.tscn")
onready var bottomExit = load("res://Scenes/NewMaze/BottomExit.tscn")

#Reference to the level exit
onready var levelExit = load("res://Scenes/Maze/Maze_Exit.tscn")

var rng = RandomNumberGenerator.new()
export var id = 1
var readyToSpawnPlayer = true
var readyHasBeenCalled = false
var mazehasBeenMade = false

var depth = 1
var tileArray = []

var left_through_exit = false
var exit_dir = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	
	print("maze has been made: ", mazehasBeenMade)
	print("left through exit: ", left_through_exit)
	
#	yield(get_tree().root, "ready")
#	set_process(true)
	rng.seed = id
	tile_size = tileMap.cell_size

	if !mazehasBeenMade:
		for x in range(maze_width):
			tileArray.append([])
			for y in range(maze_height):
				tileArray[x].append([])
		make_maze()
	else:
		for x in range(maze_width):
			for y in range(maze_height):
				 tileMap.set_cellv(Vector2(x, y), tileArray[x][y])
	readyHasBeenCalled = true
	#spawn_exit()
	readyToSpawnPlayer = true
	if(active):
		
		if(subMazes == null or subMazes.size() == 0):
			spawn_sub_maze()
		else:
			print("Initializing existing mazes")
			init_sub_mazes()
		spawn_maze_exits()
		
	
	if(parent_maze == null):
		var exit = levelExit.instance()
		exit.position = Vector2((xf*tile_size.x)+tile_size.x/2,yf*tile_size.y+tile_size.y/2)
		add_child(exit)

func _process(delta):
	
	if(id == 1):
		pass
	
	if(readyHasBeenCalled):
		if active and readyToSpawnPlayer:
			#print("MAZE ",id," is active and spawning a player")
			spawn_player()
		elif !active and player != null:
			print("deleting player")

func spawn_maze_exits():
	#Calculate our right and left shapes
	var lr_shape = RectangleShape2D.new()
	lr_shape.set_extents(Vector2(self.maze_path_wall_thickness,(-1*self.maze_height*self.tile_size.y)/2))
	var x_midpoint = Vector2(.5 * self.maze_width*self.tile_size.x,self.position.y)
	var y_midpoint = Vector2(self.position.x,.5 * self.maze_height*self.tile_size.y)
	var tb_shape = RectangleShape2D.new()
	tb_shape.set_extents(Vector2((self.maze_width*self.tile_size.y)/2,self.maze_path_wall_thickness))
	
	
	#Add the right-side exit
	var right_exit = rightExit.instance()
	right_exit.position = Vector2((self.tile_size.x * self.maze_width)+self.position.x+self.maze_path_wall_thickness,self.position.y)
	right_exit.get_node("Area/CollisionShape").set_shape(lr_shape)
	right_exit.get_node("Area/CollisionShape").position = y_midpoint
	add_child(right_exit)
	
	#Add the left-side exit
	var left_exit = leftExit.instance()
	left_exit.position = Vector2(self.position.x-self.maze_path_wall_thickness,self.position.y)
	left_exit.get_node("Area/CollisionShape").set_shape(lr_shape)
	left_exit.get_node("Area/CollisionShape").position = y_midpoint
	add_child(left_exit)
	
	#Add the top-side exit
	var top_exit = topExit.instance()
	top_exit.position = Vector2(self.position.x,self.position.y-self.maze_path_wall_thickness)
	top_exit.get_node("Area/CollisionShape").set_shape(tb_shape)
	top_exit.get_node("Area/CollisionShape").position = x_midpoint
	add_child(top_exit)
	
	#Add the top-side exit
	var bottom_exit = bottomExit.instance()
	bottom_exit.position = Vector2(self.position.x,self.position.y+self.maze_path_wall_thickness + (self.maze_height*self.tile_size.y))
	bottom_exit.get_node("Area/CollisionShape").set_shape(tb_shape)
	bottom_exit.get_node("Area/CollisionShape").position = x_midpoint
	add_child(bottom_exit)
	
	
func spawn_player():
	if(player == null):
		player = Player.instance()
		if left_through_exit:
			set_player_position(Vector2(xf, yf))
		else:
			set_player_position(Vector2(x0, y0))
		add_child(player)
	else:
		var player_temp = player.duplicate_player()
		
		player.queue_free()
		player = player_temp
		#set_player_position(Vector2(x0,y0))
		add_child(player)
	readyToSpawnPlayer = false
	
func set_player_position(v):
	#print(v.x)
	player.position.x = v.x * tile_size.x + tile_size.x/2
	player.position.y = v.y * tile_size.y + tile_size.y/2	

func set_absolute_player_position(pos : Vector2):
	if(player == null):
		spawn_player()
		readyToSpawnPlayer = false
	player.position = pos
	
func get_random_path_end():
	var targetPath = paths[rng.randi() % paths.size()]
	var endNode = targetPath[targetPath.size()-1]
	return endNode

func get_random_starting_location_on_path_to_exit(path_to_exit_spots):

	
	var invalid = true
	var index = 0
	while invalid:		
		index = rng.randi() % path_to_exit.size()		
		if(!(index == 0 or index == path_to_exit.size()-1) or path_to_exit_spots[index - 1] or path_to_exit_spots[index + 1]):
			invalid = false		
	return path_to_exit[index]
	
func get_random_cell_on_side_of_maze(flag, submaze_cell, submaze_width, submaze_height):
	
	#flag is negative 1 for exit and positive 1 for entrance
	
	var cell_nextTo_submaze = Vector2(0,0)
	for i in range(path_to_exit.size()):
		if path_to_exit[i] == submaze_cell:
			cell_nextTo_submaze = path_to_exit[i-flag]
	var dir = cell_nextTo_submaze - submaze_cell
	# if dir = (0, 1): entrance wall is bottom
	# if dir = (0, -1): entrance wall is top
	# if dir = (1, 0): entrance wall is right
	# if dir = (-1, 0): entrance wall is left
	
	var directions = [Vector2(0, 1), Vector2(0, -1), Vector2(1, 0), Vector2(-1, 0)]
	var start_y = 0
	var start_x = 0
	if(dir == directions[0]):
		start_y = submaze_height - 1
		start_x = rng.randi() % submaze_width
	elif(dir == directions[1]):	
		start_y = 0
		start_x = rng.randi() % submaze_width
	elif(dir == directions[2]):
		start_x = submaze_width - 1
		start_y = rng.randi() % submaze_height
	elif(dir == directions[3]):
		start_x = 0
		start_y = rng.randi() % submaze_height
	else:
		print("random maze entrance borken")
	return [Vector2(start_x,start_y), dir]
	
	
	
#func spawn_exit():
#	var newExit = mazeExit.instance()
#	var exitLocation = Vector2(xf,yf)
#	newExit.position.x = self.position.x+ceil(tile_size.x/2) + (tile_size.x * exitLocation.x)
#	newExit.position.y = self.position.y+ceil(tile_size.x/2) + (tile_size.y * exitLocation.y)
#	add_child(newExit)
		
func spawn_sub_maze():
	var submaze_width = int(floor(maze_width / 1.5))
	var submaze_height = int(floor(maze_height / 1.5))

	var path_to_exit_spots = path_to_exit.duplicate()
	for e in range(path_to_exit_spots.size()):
		path_to_exit_spots[e] = 0
		
	path_to_exit_spots[0] = 1
	path_to_exit_spots[path_to_exit_spots.size()-1] = 1


	for i in range(get_num_sub_mazes()):
		if(!path_is_full(path_to_exit_spots)):
			var subMaze = viewportMaze.instance()
			#var spawnLocation = get_random_path_end()
			var spawnLocation = get_random_starting_location_on_path_to_exit(path_to_exit_spots)
			

			var a = get_random_cell_on_side_of_maze(1, spawnLocation, submaze_width, submaze_height)
			#given the location of the submaze relative to the path, set the entrance to be on the face towards the starting location
			var entrance_cell = a[0]
			var dir_entrance = a[1]
			
			var b = get_random_cell_on_side_of_maze(-1, spawnLocation, submaze_width, submaze_height)
			var exit_cell = b[0]
			var dir_exit = b[1]
			subMaze.get_node("Viewport").get_node("Maze").exit_dir = dir_exit
			#set to place on path to exit
			subMaze.get_node("Viewport").get_node("Maze").set_Maze_Start_Position(entrance_cell)
			subMaze.get_node("Viewport").get_node("Maze").set_Maze_Exit_Position(exit_cell)
				
			
			subMaze.get_node("Viewport").get_node("Maze").parent_maze = self
			subMaze.get_node("Viewport").get_node("Maze").id = rng.randi()
			subMaze.get_node("Viewport").get_node("Maze").maze_width = submaze_width
			subMaze.get_node("Viewport").get_node("Maze").maze_height = submaze_height
			subMaze.set_position(Vector2(self.position.x + maze_path_wall_thickness  + (tile_size.x * spawnLocation.x),self.position.y + maze_path_wall_thickness + (tile_size.y * spawnLocation.y)))
			

			
			add_child(subMaze)
			
			var temp_paths = subMaze.get_node("Viewport").get_node("Maze").paths.duplicate()
			var temp_path_to_exit = []
			var notFound = true


			for e in temp_paths:
				for j in range(e.size()):
					if e[j] == exit_cell and notFound:
						for k in range(j + 1):
							temp_path_to_exit.append(e[k])
							notFound = false


			subMaze.get_node("Viewport").get_node("Maze").path_to_exit = temp_path_to_exit
			
			
			subMaze.get_node("Viewport").get_node("Maze").remove_wall_from_Tile(entrance_cell, dir_entrance)
			subMaze.get_node("Viewport").get_node("Maze").remove_wall_from_Tile(exit_cell, dir_exit)
			#print("SubMaze with ID:",subMaze.get_maze().id)
			subMazes.append(subMaze)
			
func init_sub_mazes():
	var submaze_width = int(floor(maze_width / 1.5))
	var submaze_height = int(floor(maze_height / 1.5))

	var path_to_exit_spots = path_to_exit.duplicate()
	for e in range(path_to_exit_spots.size()):
		path_to_exit_spots[e] = 0
	path_to_exit_spots[0] = 1
	path_to_exit_spots[path_to_exit_spots.size()-1] = 1
	
	#print(get_num_sub_mazes())
	
	for i in range(get_num_sub_mazes()):
		if(!path_is_full(path_to_exit_spots)):
			#var subMaze = viewportMaze.instance()
			var test = viewportMaze.instance()
			test.set_maze(subMazes[i])
			test.rect_position = subMazes[i].rect_position
			var subMaze = test
			#var spawnLocation = get_random_path_end()
#			var spawnLocation = get_random_starting_location_on_path_to_exit(path_to_exit_spots)
#
#
#			var a = get_random_cell_on_side_of_maze(spawnLocation, submaze_width, submaze_height)
#			#given the location of the submaze relative to the path, set the entrance to be on the face towards the starting location
#			var entrance_cell = a[0]
#			print(entrance_cell)
#			var dir = a[1]
#			#set to place on path to exit
#			subMaze.get_node("Viewport").get_node("Maze").set_Maze_Start_Position(entrance_cell)
#			subMaze.get_node("Viewport").get_node("Maze").parent_maze = self
			var viewport_maze = subMaze.get_node("Viewport").get_node("Maze")
			var target_submaze = subMazes[i].get_maze()
			viewport_maze.id = target_submaze.id
			viewport_maze.maze_width = target_submaze.maze_width
			viewport_maze.maze_height = target_submaze.maze_height
			viewport_maze.x0 = target_submaze.x0
			viewport_maze.y0 = target_submaze.y0
			viewport_maze.xf = target_submaze.xf
			viewport_maze.yf = target_submaze.yf
			viewport_maze.parent_maze = target_submaze.parent_maze
			viewport_maze.path_to_exit = target_submaze.path_to_exit
			viewport_maze.active = false
			viewport_maze.readyHasBeenCalled = false
			viewport_maze.mazehasBeenMade = true
			viewport_maze.tileArray = target_submaze.tileArray
			
			print("Inside submaze init: ", target_submaze.left_through_exit)
			
			viewport_maze.left_through_exit = target_submaze.left_through_exit
			
			viewport_maze.exit_dir = target_submaze.exit_dir
			add_child(subMaze)
			viewport_maze.tileMap = target_submaze.tileMap
			# subMaze.get_node("Viewport").get_node("Maze").remove_wall_from_Tile(entrance_cell, dir)
			#print("SubMaze with ID:",subMaze.get_maze().id)
			#subMazes.append(subMaze.get_maze())
	
func path_is_full(path_array):
	

	var max_dist_between_invalid_spots = 0
	var count = 0
	# 0 = false
	for e in path_array:
		if(!e):
			count += 1
		else:
			count = 0
		if(count > max_dist_between_invalid_spots):
			max_dist_between_invalid_spots = count
	return max_dist_between_invalid_spots < 2
			
	
	
func check_neightbors(cell, unvisited):
	#returns an array of cell's unvisited neighbors
	var list = []
	for n in cell_walls.keys():
		if cell + n in unvisited:
			list.append(cell + n)
	return list

# Get the tile id from the TileSet for the given name
func get_tile_by_name(name):
	return tileMap.tile_set.find_tile_by_name(name)

# Get the tile name under a given id
func get_tile_name_by_id(id):
	return tileMap.tile_set.tile_get_name(id)

func set_Maze_Start_Position(v):
	x0 = v.x
	y0 = v.y
	
func set_Maze_Exit_Position(v):
	xf = v.x
	yf = v.y
	
func make_maze():
	
	#Setup variables
	var unvisited = []
	var stack = []
	tileMap.clear()
	
	# fill unvisited with every cell and set them all the the default tile
	for x in range(maze_width):
		for y in range(maze_height):
			unvisited.append(Vector2(x,y))
			tileMap.set_cellv(Vector2(x,y),get_tile_by_name(str(N|S|E|W)))
	#Set out current as origin and mark as visited
	var current = Vector2(x0,y0)
	unvisited.erase(current)
	
	var new_path = true
	var temp_current = current
	var paths_index = 0
	
	paths[paths_index] = [current]
	
	while unvisited:
		
		# Grab the list of neighbors
		var neighbors = check_neightbors(current,unvisited)
		
		if neighbors.size() > 0:
			new_path = true
			#The next node we are going to try
			var next = neighbors[rng.randi() % neighbors.size()]
			
			#Puts our current node onto the stack
			stack.append(current)
			
			#Vector of unit len 1 point to the next cell
			var direction = next - current
			
			#The tile names of our current tile and the one we're moving to
			var currentTileName = int(get_tile_name_by_id(tileMap.get_cellv(current)))
			var nextTileName = int(get_tile_name_by_id(tileMap.get_cellv(next)))
			#This is going to be a number representing the active walls
			var current_walls = currentTileName - cell_walls[direction]
			var next_wall = nextTileName - cell_walls[-direction]
			
			self.tileMap.set_cellv(current,get_tile_by_name(str(current_walls)))
			self.tileMap.set_cellv(next,get_tile_by_name(str(next_wall)))
			
			# Move to our target next cell and mark current as visited
			current = next
			unvisited.erase(current)
			
			#Add our new current cell to the current path
			paths[paths_index].append(current)
		
		elif stack:
			#If we still have unvisited in the stack/path
			if new_path:
				#Duplicate then path at the junction under a incr path entry
				paths.append(paths[paths_index].duplicate())
				paths_index+=1
				new_path = false
				
			#Removes the end of the current dead path [backtracking]
			paths[paths_index].pop_back()
			
			current = stack.pop_back()
		
	# Set (xf, yf) to cell that has the longest path
	if parent_maze == null:
		var max_len = -1
		for path in paths:
			#print(path)
			if(path.size() > max_len):
				max_len = path.size()
				self.xf = path[path.size()-1].x
				self.yf = path[path.size()-1].y
				path_to_exit = path
	mazehasBeenMade = true
	for x in range(maze_width):
		for y in range(maze_height):
			tileArray[x][y] = tileMap.get_cellv(Vector2(x, y))
	#print("Exit Point:",xf," ",yf)
	print("END OF MAZE GEN",xf,"\t",yf)
				
func duplicate_maze():
	var newMaze = maze.instance()
	
	newMaze.mazehasBeenMade = self.mazehasBeenMade
	newMaze.tileArray = tileArray
	
	newMaze.x0 = self.x0
	newMaze.y0 = self.y0
	newMaze.parent_maze = self.parent_maze
				
			
	newMaze.maze_height = self.maze_height
	newMaze.maze_width = self.maze_width

	newMaze.xf = self.xf
	newMaze.yf = self.yf

	newMaze.cell_walls = self.cell_walls

	newMaze.tile_size = self.tile_size
	newMaze.maze_path_wall_thickness = self.maze_path_wall_thickness

	#Path tracking variables
	newMaze.paths = self.paths
	newMaze.path_to_exit = self.path_to_exit

	newMaze.player = self.player

	newMaze.rng = self.rng
	newMaze.readyToSpawnPlayer = self.readyToSpawnPlayer
	newMaze.id = self.id

	newMaze.tileMap = self.tileMap
	
	#Pretty sure this is the problem?
	newMaze.subMazes = self.subMazes
	for i in range(newMaze.subMazes.size()):
		newMaze.subMazes[i].get_node("Viewport").get_node("Maze").tileArray = self.subMazes[i].get_node("Viewport").get_node("Maze").tileArray
	
	newMaze.maze_height = self.maze_height
	newMaze.maze_width = self.maze_width
	
	newMaze.left_through_exit = self.left_through_exit
	newMaze.exit_dir = self.exit_dir
	
	
	return newMaze


func remove_wall_from_Tile(cell, dir):	
	# if dir = (0, 1): wall is bottom
	# if dir = (0, -1): wall is top
	# if dir = (1, 0): wall is right
	# if dir = (-1, 0): wall is left
	var TileName = int(get_tile_name_by_id(tileMap.get_cellv(cell)))
	var walls = TileName - cell_walls[dir]
	self.tileMap.set_cellv(cell, get_tile_by_name(str(walls)))
	self.tileArray[cell.x][cell.y] = self.tileMap.get_cellv(cell)

func set_tile_Map(new_tile_Map):
	for x in range(maze_width):
		for y in range(maze_height):
			self.tileMap.set_cellv(Vector2(x,y), new_tile_Map.get_cell(x, y))
			self.tileArray[x][y] = self.tileMap.get_cellv(Vector2(x, y))
			
func get_tile_Map():
	return self.tileMap
	
func get_num_sub_mazes():
	if(maze_height == 4):
		return 0
	elif(maze_height == 6):		
		return 1
	elif(maze_height == 9):	
		return 2
	elif(maze_height == 14 or 21):
		return 3	
	elif(maze_height == 32 or 48):
		return 4
	else:
		print("fucky wucky on num sub mazes")
		return 0
		
		
	
