extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var dir = Vector2(-1, 0)

func _on_Area_body_entered(body):
	if body.name == "Player":
		var MainScene = get_tree().get_root().get_node("MainScene")
		var smallMaze = MainScene.get_node("Maze")
		if smallMaze != null:
			MainScene.remove_child(smallMaze)
			var parentMaze = smallMaze.parent_maze.duplicate_maze()
			parentMaze.active = true
			MainScene.add_child(parentMaze)
			var subMazeLocation = _find_submaze_by_id(parentMaze,smallMaze.id).rect_position
			parentMaze.set_absolute_player_position(Vector2(subMazeLocation.x - (parentMaze.tile_size.x/2) - parentMaze.maze_path_wall_thickness, subMazeLocation.y + (parentMaze.tile_size.y/2) - parentMaze.maze_path_wall_thickness))
			
			
			print("Leaving Maze from Right Side")
			print("right dir: ", dir)
			print("maze_exit dir: ", _find_submaze_by_id(parentMaze,smallMaze.id).maze.exit_dir)
			if(dir == _find_submaze_by_id(parentMaze,smallMaze.id).maze.exit_dir):
				_find_submaze_by_id(parentMaze,smallMaze.id).maze.left_through_exit = true
				print("submaze left through exit: ", _find_submaze_by_id(parentMaze,smallMaze.id).maze.left_through_exit)
			else:
				_find_submaze_by_id(parentMaze,smallMaze.id).maze.left_through_exit = true
				print("submaze left through exit: ", _find_submaze_by_id(parentMaze,smallMaze.id).maze.left_through_exit)
func _find_submaze_by_id(maze,id):
	for m in maze.subMazes:
		if m.get_maze().id == id:
			return m
	return null
