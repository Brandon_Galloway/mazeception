extends ViewportContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Called when the node enters the scene tree for the first time.
export onready var viewport = $Viewport
export onready var maze = $Viewport/Maze

func _ready():
	pass # Replace with function body.

func get_maze():
	return maze
	
func set_maze(newMaze):
	add_child(newMaze)
#	var temp_maze = newMaze.duplicate_maze()
#	remove_child(maze)
#	if(maze != null):
#		maze.queue_free()
#	add_child(temp_maze)

func duplicate_subMazeCamera():
	var duplicate = self.duplicate()
	duplicate.set_maze(duplicate.get_maze())
	
	
	return duplicate
	
