tool

extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
onready var viewportMaze = get_node("../Viewport/Maze")

func _on_SubMazeArea_body_entered(body):
	print(body)
	if "Player" in body.name:
		var MainScene = get_tree().get_root().get_node("MainScene")
		var bigMaze = MainScene.get_node("Maze")
		if bigMaze != null:
			#small maze is now the viewportmaze
			
			print("Viewport maze has been made: ", viewportMaze.mazehasBeenMade)
			print("Viewport maze left through exit: ", viewportMaze.left_through_exit)
			var smallMaze = viewportMaze.duplicate_maze()
			print("Entering viewport maze")
			
			MainScene.remove_child(bigMaze)
			smallMaze.active = true
			MainScene.add_child(smallMaze)
	
