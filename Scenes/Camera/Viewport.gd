extends Viewport


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var maze = $Maze

# Called when the node enters the scene tree for the first time.
func _ready():
	var tile_size = maze.tile_size
	self.size = Vector2(tile_size.x*maze.maze_width,tile_size.y*maze.maze_height)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
