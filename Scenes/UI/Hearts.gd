extends Control


var hearts = 4 setget set_hearts
var max_hearts = 4 setget set_max_hearts

onready var label = $Label
onready var EmptyHearts = $EmptyHearts
onready var FullHearts = $FullHearts

func set_hearts(value):
	hearts = clamp(value,0,max_hearts)
	
	if FullHearts != null:
		FullHearts.rect_size.x = hearts * 32;
	
func set_max_hearts(value):
	max_hearts = max(value,1)
	if EmptyHearts != null:
		EmptyHearts.rect_size.x = max_hearts * 32

func _ready():
	self.max_hearts = 5
	self.hearts = self.max_hearts-1.5
	
