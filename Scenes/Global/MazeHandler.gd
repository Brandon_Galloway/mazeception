extends Node

class_name MazeHandler


var tile_array_name_indexed = []
var Map = null
var current_maze = null
var id = 0
var tile_size = 0

func _init(Map):
	
	self.Map = Map
	tile_size = Map.cell_size
	var ids = Map.tile_set.get_tiles_ids()
	var maxVal = 0
	
	for id in ids:
		if(int(Map.tile_set.tile_get_name(id)) > maxVal):
			maxVal = int(Map.tile_set.tile_get_name(id))
			
	for x in range(maxVal + 1):
		tile_array_name_indexed.append([])
	
	
	for id in ids:
		tile_array_name_indexed[int(Map.tile_set.tile_get_name(id))] = id


func get_Tile_Array_Name_Indexed():
	return tile_array_name_indexed

func createMaze(width, height, x0, y0, parent_maze):
	current_maze = Maze.new(width, height, x0, y0, tile_array_name_indexed, Map.duplicate(), self, parent_maze, id)
	id += 1
	
func get_Current_Maze():
	return current_maze

func set_Current_Maze(maze):
	current_maze = maze

func get_Position_Cell_In_Tiles(v):
	return v*tile_size + tile_size/2
