extends KinematicBody2D


var speed : int = 250

var vel : Vector2 = Vector2()
onready var sprite : AnimatedSprite = get_node("AnimatedSprite")

var contact_with_entrace = false
var contact_with_exit = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass

#temp
onready var mazeScene = load("res://Scenes/NewMaze/NewMaze.tscn")
onready var SubMazeCamera = load("res://Scenes/Camera/SubMazeCamera.tscn")	

#up, Right, Down, Left
var directions = [Vector2(0, -1), Vector2(1, 0), Vector2(0, 1), Vector2(-1, 0)]
var lastDir = directions[0]
var time_since_frame_start = 0

var attacking = false
var picking_up_item = false
var takenDamage = false

var frame_time = 1
func _physics_process(delta):
	vel.x = 0
	vel.y = 0
	var moving = false
	
	if(picking_up_item):
		sprite.play("PickUp")
		
	
	if(Input.is_action_just_pressed("ui_page_down")):
		self.position = Vector2(-30,0)
		#newMaze.position.x = self.position.x
		#newMaze.position.y = self.position.y
	#Attack
	if(Input.is_action_just_pressed("ui_select")):
#		if !attacking:
#			attacking = true
#			sprite.play("Attack")

		if !takenDamage:
			takenDamage = true
			sprite.play("TakenDamage")
	
	if Input.is_action_pressed("ui_left"):
		vel.x -= speed
		moving = true

	if Input.is_action_pressed("ui_right"):
		vel.x += speed
		moving = true
		
	if Input.is_action_pressed("ui_up"):
		vel.y -= speed
		moving = true

	if Input.is_action_pressed("ui_down"):
		vel.y += speed
		moving = true

	if(vel.y != 0 and vel.x != 0):
		vel.y += sqrt(speed) * (vel.y / abs(vel.y))
		vel.x += sqrt(speed) * (vel.x / abs(vel.x))
	
	if(picking_up_item):
		vel.x = 0
		vel.y = 0
	
	vel = move_and_slide(vel,Vector2.UP)
	
	if !moving and !attacking and !picking_up_item and !takenDamage:
		sprite.play("Default")

	if !attacking and !picking_up_item and !takenDamage:
		sprite.play("Walk")		
		if(vel.x > 0):
			sprite.rotation = 0
		elif(vel.x < 0):
			sprite.rotation = PI
		elif(vel.y > 0):
			sprite.rotation = PI/2
		elif(vel.y < 0):
			sprite.rotation = 3*PI/2
			
			
	if attacking and !picking_up_item and !takenDamage:
		time_since_frame_start += delta
		if(time_since_frame_start > frame_time):
			attacking = false
			time_since_frame_start = 0
			
	if picking_up_item:
		time_since_frame_start += delta
		if(time_since_frame_start > frame_time):
			picking_up_item = false
			time_since_frame_start = 0
		
	if takenDamage:
		time_since_frame_start += delta
		if(time_since_frame_start > frame_time):
			takenDamage = false
			time_since_frame_start = 0	
		
		
		
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if(collision.collider.name == "Sub_Maze_Entrance"):
			contact_with_entrace = true
		elif(collision.collider.name == "Maze_Exit"):
			contact_with_exit = true
	

#	if vel.x < 0:
#		sprite.flip_h = true
#	else:
#		sprite.flip_h = false
		

func duplicate_player():
	var duplicate = self.duplicate()
	duplicate.position = self.position
	return duplicate

